---
title: "Черепаха"
tags:
    - user manual
    - utility
    - goatcounter
date: "2024-03-02"
thumbnail: "/assets/img/thumbnail/turtle.jpeg"
bookmark: true
---

<img alt="goatcounter" src="/assets/img/thumbnail/turtle.jpeg"/>

[![goatcounter](https://cdn.icon-icons.com/icons2/2699/PNG/512/goatcounter_logo_icon_170078.png){:class="img-md"}](https://www.goatcounter.com/)


GoatCounter is an open source **web analytics platform** available as a free donation-supported hosted service or self-hosted app. It aims to offer easy to use and meaningful privacy-friendly web analytics as an alternative to Google Analytics or Matomo (*in official guide*).

# Getting started
---

## Sign up & Get your access code

Create new account in [here](https://www.goatcounter.com/signup)

You will access your blog statistics at `https://[my-code].goatcounter.com`.

## Add your code to `_config.yml`

```
goatcounter_code: [my-code]
```

Check if your adblocker is blocking GoatCounter if you don’t see any pageviews.

## Allow visitor counter

Sign in **Goat counter** from your browser, and enter the [Settings](https://[my-code].goatcounter.com/settings/main) tab.

Make sure `Allow adding visitor counts on your website` is checked:

![](https://i.ibb.co/R7TKCmy/2024-01-13-043651.png){:class="img-md"}

